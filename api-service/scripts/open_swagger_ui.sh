#!/bin/sh
#
# Usage: run from project directory: ./scripts/open_swagger_editor.sh
# Description: run docker machine with openapi.yaml & open browser with swagger editor
# Requirements: docker
#

name='swagger-ui'
command -v docker >/dev/null 2>&1 || { echo >&2 "'docker' is not install installed. Aborting."; exit 1; }
[[ $(docker ps -f "name=$name" --format '{{.Names}}') == $name ]] ||
docker run --rm -d -p 8045:8080 --name "$name" -e SWAGGER_JSON=/config/openapi.yaml -v $(PWD)/config:/config swaggerapi/swagger-ui

#podman run --rm -d -p 8044:8080 swaggerapi/swagger-editor
# run swagger-editor container with the yaml, if not running yet
# opening specific file doesn't work with editor
# podman run --rm -d -p 8044:8080 --name "$name" -e SWAGGER_JSON=/config/openapi.yaml -v $(PWD)/config:/config swaggerapi/swagger-editor


# open swagger-ui in browser
open http://localhost:8045
