/* backend user rest api
Inspired by tutorials from Alex Losikov
Link: https://losikov.medium.com/

Refactoring and extending api calls by Jenny Pilz
See the changes based on the commits
*/

import database from '@mf/utils/database.utils'
import apiConfig from '@mf/config/app.config'
import { createApiServer } from '@mf/utils/server.util'
import LOGGER from '@mf/utils/logger_level.utils'

const API_URL = apiConfig.apiServerSettings.url
const PORT = apiConfig.apiServerSettings.port
const ERROR_LABEL = 'Error'
const LISTENER_MESSAGE = 'Listening on '

database.open()
  .then(() => createApiServer())
  .then(apiServer => {
    apiServer.listen(PORT, () => {
      LOGGER.info(`${LISTENER_MESSAGE}`+API_URL+PORT)
    })
  })
  .catch(error => {
    LOGGER.error("Mongo geschlossen")
    LOGGER.error(`${ERROR_LABEL}: ${error}`)
  })
