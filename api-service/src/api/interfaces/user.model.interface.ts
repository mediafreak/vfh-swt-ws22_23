import { Document } from 'mongoose';

export interface IUserDocument extends Document {
  password: string;
  email: string;
  firstName: string;
  lastName: string;
  created: Date;
}

export interface IUser extends IUserDocument {
  comparePassword(password: string): Promise<boolean>;
}
