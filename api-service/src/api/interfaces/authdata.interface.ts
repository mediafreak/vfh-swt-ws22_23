interface IAuthData {
    validTokenSchema: string
    invalidTokenSchema: string
    validToken: string
    invalidToken: string
    validUserID: string
    authType: string 
    authFailedMessage: string 
  }

export const AUTH_DATA: IAuthData = {
    validTokenSchema: 'Bearer ',
    invalidTokenSchema: 'Bearer Bonbondose ',
    validToken: 'fakeToken',
    invalidToken: 'invalidToken',
    validUserID: 'fakeUserId',
    authType: 'unauthorized',
    authFailedMessage: 'Authentication Failed'
  }