interface IErrorTypes {
  invalidCredentials: { type: string; message: string };
  internalServerError: { type: string; message: string };
  authenticationFailed: { type: string; message: string };
}

export const ERROR_TYPES: IErrorTypes = {
  invalidCredentials: {
    type: "invalid_credentials",
    message: "Invalid Login/Password",
  },
  internalServerError: {
    type: "internal_server_error",
    message: "Internal Server Error",
  },
  authenticationFailed: {
    type: "unauthorized",
    message: "Authentication Failed",
  },
};
