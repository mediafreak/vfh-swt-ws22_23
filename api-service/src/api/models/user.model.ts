import bcrypt from "bcrypt";
import { Schema, model, Model } from "mongoose";

import validator from "validator";
import { IUser, IUserDocument } from "@mf/api/interfaces/user.model.interface";

const userSchema = new Schema<IUser>(
  {
    password: { type: String, required: true },
    email: {
      type: String,
      required: true,
      trim: true,
      validate: [validator.isEmail, "do not match email regex"],
    },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    created: { type: Date, default: Date.now },
  },
  { strict: true }
).index(
  { email: 1 },
  { unique: true, collation: { locale: "en_US", strength: 1 }, sparse: true }
);

userSchema.pre<IUserDocument>("save", function (next): void {
  if (this.isModified("password")) {
    // generate hash for password
    bcrypt.genSalt(10, (err, salt) => {
      /* istanbul ignore next */
      if (err) return next(err);
      bcrypt.hash(this.password, salt, (err, hash) => {
        /* istanbul ignore next */
        if (err) return next(err);
        this.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

userSchema.set("toJSON", {
  transform: function (doc, ret, options) {
    ret.created = ret.created.getTime();

    delete ret.__v;
    delete ret._id;
    delete ret.password;
  },
});

userSchema.methods.comparePassword = function (
  _candidatePassword: string
): Promise<boolean> {
  const { password } = this;
  return new Promise(function (resolve, reject) {
    bcrypt.compare(_candidatePassword, password, function (error, isMatch) {
      /* istanbul ignore next */
      if (error) return reject(error);
      return resolve(isMatch);
    });
  });
};

export interface IUserModel extends Model<IUser> {
  // collection/documents level operations (fetch one or many, update, save back to db)
}

export const User: IUserModel = model<IUser, IUserModel>("User", userSchema);

export default User;
