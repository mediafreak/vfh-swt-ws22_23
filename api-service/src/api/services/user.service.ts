import User from "@mf/api/models/user.model";
import LOGGER from "@mf/utils/logger_level.utils";

import { AUTH_DATA } from "@mf/api/interfaces/authdata.interface";
import { ERROR_TYPES } from "../interfaces/errorTypes.interface";

export type ErrorResponse = { error: { type: string; message: string } };
export type AuthResponse = ErrorResponse | { userId: string };
export type CreateUserResponse = ErrorResponse | { userId: string };
export type AuthCreateUserResponse = ErrorResponse | { userId: string };

// https://www.predic8.de/bearer-token-autorisierung-api-security.htm
// https://swagger.io/docs/specification/authentication/bearer-authentication/
function auth(_bearerToken: string): Promise<AuthResponse> {
  return new Promise(function (resolve, reject) {
    const AUTH_TOKEN = _bearerToken.replace(AUTH_DATA.validTokenSchema, '')
    if (AUTH_TOKEN === AUTH_DATA.validToken) {
      resolve({ userId: AUTH_DATA.validUserID});
      return;
    }
    resolve({
      error: ERROR_TYPES.authenticationFailed,
    });
  });
}

function createUser(
  email: string,
  password: string,
  firstName: string,
  lastName: string
): Promise<CreateUserResponse> {
  return new Promise(function (resolve, reject) {
    const USER_IS_CREATED = new User({ email: email, password: password, firstName: firstName, lastName: lastName });
    USER_IS_CREATED
      .save()
      .then((u) => {
        resolve({ userId: u._id.toString() });
      })
      .catch((error) => {
        if (error.code === 11000) {
          resolve({
            error: {
              type: "account_already_exists",
              message: `${email} already exists`,
            },
          });
        } else {
          LOGGER.error(`createUser: ${error}`);
          reject(error);
        }
      });
  });
}

export default { auth: auth, createUser: createUser};
