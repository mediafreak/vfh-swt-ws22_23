import { faker } from "@faker-js/faker";
import { IUser } from "@mf/api/interfaces/user.model.interface";

import User from "@mf/api/models/user.model";
import database from "@mf/utils/database.utils";

beforeAll(async () => {
  await database.open();
});

afterAll(async () => {
  await database.close();
});

describe("save", () => {
  it("should create user", async () => {
    const EMAIL = faker.internet.email();
    const PASSWORD = faker.internet.password();
    const FIRST_NAME = faker.name.firstName(); 
    const LAST_NAME = faker.name.lastName();
    const DATE_BEFORE = Date.now();

    const USER_IS_CREATED = new User({ email: EMAIL, password: PASSWORD, firstName:FIRST_NAME, lastName:LAST_NAME });
    await USER_IS_CREATED.save();

    const DATE_AFTER = Date.now();

    const USER_FETCHED = await User.findById(USER_IS_CREATED._id);

    expect(USER_FETCHED).not.toBeNull();

    expect(USER_FETCHED!.email).toBe(EMAIL);
    expect(USER_FETCHED!.firstName).toBe(FIRST_NAME);
    expect(USER_FETCHED!.lastName).toBe(LAST_NAME);
    expect(USER_FETCHED!.password).not.toBe(PASSWORD);

    expect(DATE_BEFORE).toBeLessThanOrEqual(USER_FETCHED!.created.getTime());
    expect(USER_FETCHED!.created.getTime()).toBeLessThanOrEqual(DATE_AFTER);
  });

  it("should update user", async () => {
    const FIRST_NAME_1 = faker.name.firstName();
    const USER_IS_UPDATED = new User({
      email: faker.internet.email(),
      password: faker.internet.password(),
      firstName:FIRST_NAME_1,
      lastName:faker.name.lastName(),
    });
    const USER_IN_DATABASE_1 = await USER_IS_UPDATED.save();

    const FIRST_NAME_2 = faker.name.firstName();
    const LAST_NAME_2 = faker.name.lastName();
    USER_IN_DATABASE_1.firstName = FIRST_NAME_2;
    USER_IN_DATABASE_1.lastName = LAST_NAME_2;
    const USER_IN_DATABASE_2 = await USER_IN_DATABASE_1.save();
    expect(USER_IN_DATABASE_2.firstName).toEqual(FIRST_NAME_2);
    expect(USER_IN_DATABASE_2.lastName).toEqual(LAST_NAME_2);
  });

  it("should not save user with invalid email", async () => {
    const USER_WITH_INVALID_MAIL = new User({
      email: "email@em.o",
      password: faker.internet.password(),
    });
    await expect(USER_WITH_INVALID_MAIL.save()).rejects.toThrowError(/do not match email regex/);
  });

  it("should not save user without an email", async () => {
    const USER_WITHOUT_MAIL = new User({
      password: faker.internet.password(),
      firstName:faker.name.firstName(),
      lastName:faker.name.lastName(),
    });
    await expect(USER_WITHOUT_MAIL.save()).rejects.toThrowError(/email/);
  });

  it("should not save user without a password", async () => {
    const USER_WITHOUT_PASSWORD = new User({
      email: faker.internet.email(),
      firstName:faker.name.firstName(),
      lastName:faker.name.lastName(),
    });
    await expect(USER_WITHOUT_PASSWORD.save()).rejects.toThrowError(/password/);
  });

  it("should not save user without a name", async () => {
    const USER_WITHOUT_NAME = new User({
      email: faker.internet.email(),
      password: faker.internet.password(),
    });
    await expect(USER_WITHOUT_NAME.save()).rejects.toThrowError(/lastName/);
    await expect(USER_WITHOUT_NAME.save()).rejects.toThrowError(/firstName/);
  });

  it("should not save user with the same email", async () => {
    const EMAIL = faker.internet.email();
    const PASSWORD = faker.internet.password();
    const FIRST_NAME = faker.name.firstName(); 
    const LAST_NAME = faker.name.lastName();
    const USER_DATA = { email: EMAIL, password: PASSWORD, firstName:FIRST_NAME, lastName:LAST_NAME };

    const USER_EXISTING = new User(USER_DATA);
    await USER_EXISTING.save();

    const USER_WITH_SAME_MAIL = new User(USER_DATA);
    await expect(USER_WITH_SAME_MAIL.save()).rejects.toThrowError(/E11000/);
  });

  it("should not save password in a readable form", async () => {
    const READABLE_PASSWORD = faker.internet.password();

    const USER_1 = generateUser()
    await USER_1.save();
    expect(USER_1.password).not.toBe(READABLE_PASSWORD);

    const USER_2 = generateUser()
    await USER_2.save();
    
    expect(USER_2.password).not.toBe(READABLE_PASSWORD);
    expect(USER_1.password).not.toBe(USER_2.password);

    function generateUser() {
      return new User({
        email: faker.internet.email(),
        password: READABLE_PASSWORD,
        firstName:faker.name.firstName(),
        lastName:faker.name.lastName(),
      })
    }
  });
});

describe("comparePassword", () => {
  it("should return true for valid password", async () => {
    const PASSWORD = faker.internet.password();
    const USER = new User({
      email: faker.internet.email(),
      password: PASSWORD,
      firstName:faker.name.firstName(),
      lastName:faker.name.lastName(),
    });
    await USER.save();
    expect(await USER.comparePassword(PASSWORD)).toBe(true);
  });

  it("should return false for invalid password", async () => {
    const USER = new User({
      email: faker.internet.email(),
      password: faker.internet.password(),
      firstName:faker.name.firstName(),
      lastName:faker.name.lastName(),
    });
    await USER.save();
    expect(await USER.comparePassword(faker.internet.password())).toBe(false);
  });

  it("should update password hash if password is updated", async () => {
    const OLD_PASSWORD = faker.internet.password();
    const USER = new User({
      email: faker.internet.email(),
      password: OLD_PASSWORD,
      firstName:faker.name.firstName(),
      lastName:faker.name.lastName(),
    });
    const USER_IN_DATABASE_1 = await USER.save();
    expect(await USER_IN_DATABASE_1.comparePassword(OLD_PASSWORD)).toBe(true);

    const NEW_PASSWORD = faker.internet.password();
    USER_IN_DATABASE_1.password = NEW_PASSWORD;
    const USER_IN_DATABASE_2 = await USER_IN_DATABASE_1.save();
    expect(await USER_IN_DATABASE_2.comparePassword(NEW_PASSWORD)).toBe(true);
    expect(await USER_IN_DATABASE_2.comparePassword(OLD_PASSWORD)).toBe(false);
  });
});

describe("toJSON", () => {
  it("should return valid JSON", async () => {
    const EMAIL = faker.internet.email();
    const PASSWORD = faker.internet.password();
    const FIRST_NAME = faker.name.firstName();
    const LAST_NAME = faker.name.lastName();

    const USER = new User({ email: EMAIL, password: PASSWORD, firstName:FIRST_NAME , lastName:LAST_NAME});
    await USER.save();
    expect(USER.toJSON()).toEqual({
      email: EMAIL,
      firstName:FIRST_NAME,lastName:LAST_NAME,
      created: expect.any(Number),
    });
  });
});
