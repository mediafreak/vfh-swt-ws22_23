import { faker } from "@faker-js/faker";

import database from "@mf/utils/database.utils";
import User from "@mf/api/services/user.service";
import { ERROR_TYPES } from "@mf/api/interfaces/errorTypes.interface";

beforeAll(async () => {
  await database.open();
});

afterAll(async () => {
  await database.close();
});

describe("auth", () => {
  it("should resolve with true and valid userId for hardcoded token", async () => {
    const VALID_TOKEN_RESPONSE = await User.auth("fakeToken");
    expect(VALID_TOKEN_RESPONSE).toEqual({ userId: "fakeUserId" });
  });

  it("should resolve with false for invalid token", async () => {
    const INVALID_TOKEN_RESPONSE = await User.auth("invalidToken");
    expect(INVALID_TOKEN_RESPONSE).toEqual({
      error: ERROR_TYPES.authenticationFailed,
    });
  });
});

describe("createUser", () => {
  it("should resolve with true and valid userId", async () => {
    const EMAIL = faker.internet.email();
    const PASSWORD = faker.internet.password();
    const FIRST_NAME = faker.name.firstName();
    const LAST_NAME = faker.name.lastName();

    await expect(
      User.createUser(EMAIL, PASSWORD, FIRST_NAME, LAST_NAME)
    ).resolves.toEqual({
      userId: expect.stringMatching(/^[a-f0-9]{24}$/),
    });
  });

  it("should resolves with false & valid error if duplicate", async () => {
    const DUPLICATED_EMAIL = faker.internet.email();
    const DUPLICATED_PASSWORD = faker.internet.password();
    const DUPLICATED_FIRST_NAME = faker.name.firstName();
    const DUPLICATED_LAST_NAME = faker.name.lastName();

    await User.createUser(
      DUPLICATED_EMAIL,
      DUPLICATED_PASSWORD,
      DUPLICATED_FIRST_NAME,
      DUPLICATED_LAST_NAME
    );

    await expect(
      User.createUser(
        DUPLICATED_EMAIL,
        DUPLICATED_PASSWORD,
        DUPLICATED_FIRST_NAME,
        DUPLICATED_LAST_NAME
      )
    ).resolves.toEqual({
      error: {
        type: "account_already_exists",
        message: `${DUPLICATED_EMAIL} already exists`,
      },
    });
  });

  it("should reject if invalid input", async () => {
    const EMAIL = "invalid@em.c";
    const PASSWORD = faker.internet.password();
    const FIRST_NAME = faker.name.firstName();
    const LAST_NAME = faker.name.lastName();

    await expect(
      User.createUser(EMAIL, PASSWORD, FIRST_NAME, LAST_NAME)
    ).rejects.toThrowError("validation failed");
  });
});
