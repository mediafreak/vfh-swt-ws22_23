import { faker } from "@faker-js/faker";

import request from "supertest";
import express from "express";

import database from "@mf/utils/database.utils";
import { createApiServer } from "@mf/utils/server.util";
import apiConfig from "@mf/config/app.config";

let apiServer = express();
const apiLink = apiConfig.apiLink;

beforeAll(async () => {
  await database.open();
  apiServer = await createApiServer();
});

afterAll(async () => {
  await database.close();
});

describe("POST " + apiLink + "/public/user", () => {
  it("should return 201 & valid response for valid user", (done) => {
    request(apiServer)
      .post(`${apiLink}/public/user`)
      .send({
        email: faker.internet.email(),
        password: faker.internet.password(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      })
      .expect(201)
      .end(function (error, response) {
        if (error) return done(error);
        expect(response.body).toMatchObject({
          userId: expect.stringMatching(/^[a-f0-9]{24}$/),
        });
        done();
      });
  });

  it("should return 409 & valid response for duplicated user", (done) => {
    const DUPLICATED_USER_DATA = {
      email: faker.internet.email(),
      password: faker.internet.password(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
    };
    request(apiServer)
      .post(`${apiLink}/public/user`)
      .send(DUPLICATED_USER_DATA)
      .expect(201)
      .end(function (error, response) {
        if (error) return done(error);
        request(apiServer)
          .post(`${apiLink}/public/user`)
          .send(DUPLICATED_USER_DATA)
          .expect(409)
          .end(function (error, response) {
            if (error) return done(error);
            expect(response.body).toMatchObject({
              error: {
                type: "account_already_exists",
                message: expect.stringMatching(/already exists/),
              },
            });
            done();
          });
      });
  });

  it("should return 400 & valid response for invalid request", (done) => {
    request(apiServer)
      .post(`${apiLink}/public/user`)
      .send({
        mail: faker.internet.email(),
        password: faker.internet.password(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      })
      .expect(400)
      .end(function (error, response) {
        if (error) return done(error);
        expect(response.body).toMatchObject({
          error: {
            type: "request_validation",
            message: expect.stringMatching(/email/),
          },
        });
        done();
      });
  });
});
