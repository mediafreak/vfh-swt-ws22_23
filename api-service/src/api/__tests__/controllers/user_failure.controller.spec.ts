import { faker } from "@faker-js/faker";

import request from "supertest";
import express from "express";

import UserService from "@mf/api/services/user.service";
import { createApiServer } from "@mf/utils/server.util";
import { ERROR_TYPES } from "@mf/api/interfaces/errorTypes.interface";
import apiConfig from '@mf/config/app.config'
import LOGGER from "@mf/utils/logger_level.utils";
import { AUTH_DATA } from "@mf/api/interfaces/authdata.interface";

jest.mock("@mf/api/services/user.service");

let apiServer = express();
const apiLink = apiConfig.apiLink

beforeAll(async () => {
  apiServer = await createApiServer();
});

describe('auth failure', () => {
  it('should return 500 & valid response if auth rejects with an error', (done) => {
    (UserService.auth as jest.Mock).mockRejectedValue(new Error())
    request(apiServer)
      .get(`${apiLink}/goodbye`)
      .set('Authorization', AUTH_DATA.validTokenSchema+AUTH_DATA.validToken)
      .expect(500)
      .end(function(error, response) {
        if (error) return done(error)
        expect(response.body).toMatchObject({error: ERROR_TYPES.internalServerError})
        done()
      })
  })
})

describe("createUser failure", () => {
  it("should return 500 & valid response if auth rejects with an error",  (done) => {
    (UserService.createUser as jest.Mock).mockResolvedValue({
      error: { type: "unkonwn" },
    });
    request(apiServer)
      .post(`${apiLink}/public/user`)
      .send({
        email: faker.internet.email(),
        password: faker.internet.password(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
      })
      .expect(500)
      .end(function (error, response) {
        if (error) return done(error);
        expect(response.body).toMatchObject({ 
          error: ERROR_TYPES.internalServerError,
        });
        done();
      });
  });
});

