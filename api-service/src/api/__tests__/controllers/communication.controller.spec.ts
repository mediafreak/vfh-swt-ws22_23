import request from "supertest";
import express from "express";

import { createApiServer } from "@mf/utils/server.util";
import { AUTH_DATA } from "@mf/api/interfaces/authdata.interface";
import apiConfig from '@mf/config/app.config'

let apiServer = express();
const apiLink = apiConfig.apiLink

beforeAll(async () => {
  apiServer = await createApiServer();
});

describe("GET /welcome", () => {
  it("should return 200 & valid response if request param list is empity", (done) => {
    request(apiServer)
      .get(`${apiLink}/welcome`)
      .send()
      .expect("Content-Type", /json/)
      .expect(200)
      .end((error, response) => {
        if (error) return done(error);
        expect(response.body).toMatchObject({ message: "Welcome, Nerd!" });
        done();
      });
  });

  it("should return 200 & valid response if name param is set", (done) => {
    request(apiServer)
      .get(`${apiLink}/welcome?name=Test%20Name`)
      .expect("Content-Type", /json/)
      .expect(200)
      .end((error, response) => {
        if (error) return done(error);
        expect(response.body).toMatchObject({ message: "Welcome, Test Name!" });
        done();
      });
  });

  it("should return 400 & valid error response if name param is empty", (done) => {
    request(apiServer)
      .get(`${apiLink}/welcome?name=`)
      .expect("Content-Type", /json/)
      .expect(400)
      .end((error, response) => {
        if (error) return done(error);
        expect(response.body).toMatchObject({
          error: {
            type: "request_validation",
            message: expect.stringMatching(/Empty.*\'name\'/),
            errors: expect.anything(),
          },
        });
        done();
      });
  });
});

describe("GET /goodbye", () => {
  const fakeToken = AUTH_DATA.validToken
  it("should return 200 & valid response to authorization with " + fakeToken + " request", (done) => {
    request(apiServer)
      .get(`${apiLink}/goodbye`)
      .set("Authorization", "Bearer "+ fakeToken)
      .expect("Content-Type", /json/)
      .expect(200)
      .end(function (error, response) {
        if (error) return done(error);
        expect(response.body).toMatchObject({
          message: "Bye, Bye, " + AUTH_DATA.validUserID + "!",
        });
        done();
      });
  });

  it("should return 401 & valid eror response to invalid authorization token", (done) => {
    request(apiServer)
      .get(`${apiLink}/goodbye`)
      .set("Authorization", "Bearer invalidFakeToken")
      .expect("Content-Type", /json/)
      .expect(401)
      .end(function (error, response) {
        if (error) return done(error);
        expect(response.body).toMatchObject({
          error: { type: "unauthorized", message: "Authentication Failed" },
        });
        done();
      });
  });

  it("should return 401 & valid eror response if authorization header field is missed", (done) => {
    request(apiServer)
      .get(`${apiLink}/goodbye`)
      .expect("Content-Type", /json/)
      .expect(401)
      .end(function (error, response) {
        if (error) return done(error);
        expect(response.body).toMatchObject({
          error: {
            type: "request_validation",
            message: "Authorization header required",
            errors: expect.anything(),
          },
        });
        done();
      });
  });
});
