import * as express from 'express'
import { writeJsonResponse } from '@mf/utils/express.utils'

const WELCOME_MESSAGE = 'Welcome'
const GOODBYE_MESSAGE = 'Bye, Bye'
const UNAUTHORIZED_USER = 'Nerd'
 
export function welcome(_request: express.Request, _response: express.Response): void {
  const NAME = _request.query.name || UNAUTHORIZED_USER
  writeJsonResponse(_response, 200, {"message": `${WELCOME_MESSAGE}, ${NAME}!`})
}

export function goodbye(_request: express.Request, _response: express.Response): void {
  const userId = _response.locals.auth.userId
  writeJsonResponse(_response, 200, {"message": `${GOODBYE_MESSAGE}, ${userId}!`})
}