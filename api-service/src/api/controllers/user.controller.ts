import * as express from "express";

import UserService, { ErrorResponse } from "@mf/api/services/user.service";
import { writeJsonResponse } from "@mf/utils/express.utils";
import LOGGER from "@mf/utils/logger_level.utils";
import { ERROR_TYPES } from "../interfaces/errorTypes.interface";

export function auth(
  _request: express.Request,
  _response: express.Response,
  _next: express.NextFunction
): void {
  const AUTH_TOKEN = _request.headers.authorization!;
  UserService.auth(AUTH_TOKEN)
    .then((authResponse) => {
      if (!(authResponse as any).error) {
        _response.locals.auth = {
          userId: (authResponse as { userId: string }).userId,
        };
        _next();
      } else {
        writeJsonResponse(_response, 401, authResponse);
      }
    })
    .catch((_error) => {
      writeJsonResponse(_response, 500, {
        error: ERROR_TYPES.internalServerError,
      });
    });
}

export function createUser(
  _request: express.Request,
  _response: express.Response
): void {
  const { email, password, firstName, lastName } = _request.body;

  UserService.createUser(email, password, firstName, lastName)
    .then((resp) => {
      if ((resp as any).error) {
        if ((resp as ErrorResponse).error.type === "account_already_exists") {
          writeJsonResponse(_response, 409, resp);
        } else {
          throw new Error(`unsupported ${resp}`);
        }
      } else {
        writeJsonResponse(_response, 201, resp);
      }
    })
    .catch((error: any) => {
      LOGGER.error(`createUser: ${error}`);
      writeJsonResponse(_response, 500, {
        error: ERROR_TYPES.internalServerError,
      });
    });
}
