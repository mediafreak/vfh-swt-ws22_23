import dotenvExtended from "dotenv-extended";
import dotenvParseVariables from "dotenv-parse-variables";

const env = dotenvExtended.load({
  path: process.env.ENV_FILE,
  defaults: './config/.env.defaults',
  schema: './config/.env.schema',
  includeProcessEnv: true,
  silent: false,
  errorOnMissing: true,
  errorOnExtra: true
})

const parsedEnv = dotenvParseVariables(env)

type LogLevel =
  | "debug"
  | "error"
  | "http"
  | "info"
  | "silent"
  | "silly"
  | "verbose"
  | "warn";

interface Config {
  //https://mongoosejs.com/docs/guide.html#indexes
  mongo: {
    url: string;
    useCreateIndex: boolean,
    autoIndex: boolean;
  };

  morganLogger: boolean;
  morganBodyLogger: boolean;
  mfDevLogger: boolean;
  loggerLevel: LogLevel;

}

const config: Config = {
  mongo: {
    url: parsedEnv.MONGO_URL as string,
    useCreateIndex: parsedEnv.MONGO_CREATE_INDEX as boolean,
    autoIndex: parsedEnv.MONGO_AUTO_INDEX as boolean,
  },

  morganLogger: parsedEnv.MORGAN_LOGGER as boolean,
  morganBodyLogger: parsedEnv.MORGAN_BODY_LOGGER as boolean,
  mfDevLogger: parsedEnv.MF_DEV_LOGGER as boolean,
  loggerLevel: parsedEnv.LOGGER_LEVEL as LogLevel,

};

export default config;
