import dotenvExtended from "dotenv-extended";
import dotenvParseVariables from "dotenv-parse-variables";
import { Summary, summarise } from "swagger-routes-express";
import YAML_JS from "yamljs";

const env = dotenvExtended.load({
  path: process.env.ENV_FILE,
  defaults: "./config/.env.defaults",
  schema: "./config/.env.schema",
  includeProcessEnv: true,
  silent: false,
  errorOnMissing: true,
  errorOnExtra: true,
});

const parsedEnv = dotenvParseVariables(env);

const yamlSpecFile = "./config/openapi.yaml";
const yamlSpecFileLoaded = YAML_JS.load(yamlSpecFile);

const yamlSpec = {
  apiSummary: summarise(yamlSpecFileLoaded),
  apiLink: getAPILinkFromYAMLSpecFile(yamlSpecFileLoaded),
};

function getAPILinkFromYAMLSpecFile(apiDefinition: any) {
  for (const server of apiDefinition.servers) {
    return server.url;
  }
}

interface APIConfig {
  yamlSpecFile: string;
  apiDefinition: any;
  apiSummary: Summary;
  apiLink: string;
  apiServerSettings: {
    url: string;
    port: number;
  };
}

const apiConfig: APIConfig = {
  yamlSpecFile: yamlSpecFile as string,
  apiDefinition: yamlSpecFileLoaded as any,
  apiSummary: yamlSpec.apiSummary as Summary,
  apiLink: yamlSpec.apiLink as string,

  apiServerSettings: {
    url: parsedEnv.API_URL as string,
    port: parsedEnv.API_PORT as number,
  },
};

export default apiConfig;
