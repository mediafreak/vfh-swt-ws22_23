/* istanbul ignore file */

import express from 'express'
import LOGGER from '@mf/utils/logger_level.utils'
import { convertHrTimeInMilisecond } from '@mf/utils/timeCalculator.utils'

export const LOGGER_DEV_EXPRESS = (_request: express.Request, _response: express.Response, _next: express.NextFunction): void => {
  const START_HR_TIME = process.hrtime()
  const USER_AGENT = "User-Agent"
  const REQUEST_STRING = 'Request'
  const RESPONSE_STRING = 'Response'
  const BODY_STRING = 'Body'
  const TIME_UNIT = 'ms'

  LOGGER.http(`${REQUEST_STRING}: ${_request.method} ${_request.url} at ${new Date().toISOString()}, ${USER_AGENT}: ${_request.get(USER_AGENT)}`)
  LOGGER.http(`${REQUEST_STRING} ${BODY_STRING}: ${JSON.stringify(_request.body)}`)

  const [oldWrite, oldEnd] = [_response.write, _response.end]
  const chunks: Buffer[] = []
  ;(_response.write as unknown) = function(chunk: any): void {
    /* istanbul ignore file */
    chunks.push(Buffer.from(chunk))
    ;(oldWrite as Function).apply(_response, arguments)
  }

  _response.end = function(chunk: any): any {
    if (chunk) chunks.push(Buffer.from(chunk))

    const ELAPSED_HR_TIME = process.hrtime(START_HR_TIME)
    const ELAPSED_TIME_IN_MS = convertHrTimeInMilisecond(ELAPSED_HR_TIME)

    LOGGER.http(`${RESPONSE_STRING} ${_response.statusCode} ${ELAPSED_TIME_IN_MS.toFixed(3)} ${TIME_UNIT}`)

    const BODY = Buffer.concat(chunks).toString('utf8')
    LOGGER.http(`${RESPONSE_STRING} ${BODY_STRING}: ${BODY}`)
    ;(oldEnd as Function).apply(_response, arguments)
  }
  
  _next()
}
