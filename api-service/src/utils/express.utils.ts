import * as express from 'express'
import { OutgoingHttpHeaders } from 'http'

export function writeJsonResponse(_response: express.Response, _code: any, _payload: any, _headers?: OutgoingHttpHeaders | undefined): void {
  const DATA = typeof _payload === 'object' ? JSON.stringify(_payload, null, 2) : _payload
  _response.writeHead(_code, {..._headers, 'Content-Type': 'application/json'})
  _response.end(DATA)
}