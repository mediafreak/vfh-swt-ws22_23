export const convertHrTimeInMilisecond = (elapsedHrTime: [number, number] ): number => {
    return elapsedHrTime[0] * 1000 + elapsedHrTime[1] / 1e6
  }