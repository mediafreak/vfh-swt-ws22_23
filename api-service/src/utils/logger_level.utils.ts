/* istanbul ignore file */

import winston from 'winston'
import config from '@mf/config'

/* npm debug levels (winston default):
error: 0, warn: 1, info: 2, http: 3 verbose: 4, debug: 5, silly: 6 */

const SERVICE_NAME = 'api-service'
const TIMESTAMP_FORMAT = 'YYYY-MM-DD HH:mm:ss.SSS'
const LOGLEVEL_DEFAULT = 'silent'

const prettyJson = winston.format.printf(info => {
  if (info.message.constructor === Object) {
    info.message = JSON.stringify(info.message, null, 4)
  }
  return `${info.timestamp} ${info.label || '-'} ${info.level}: ${info.message}`
})

const LOGGER = winston.createLogger({
  level: config.loggerLevel === LOGLEVEL_DEFAULT ? undefined : config.loggerLevel,
  silent: config.loggerLevel === LOGLEVEL_DEFAULT,
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.prettyPrint(),
    winston.format.splat(),
    winston.format.simple(),
    winston.format.timestamp({format: TIMESTAMP_FORMAT}),
    prettyJson
  ),
  defaultMeta: {service: SERVICE_NAME},
  transports: [new winston.transports.Console({})]
})

export default LOGGER