/* istanbul ignore file */

import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

import config from "@mf/config";
import LOGGER from "@mf/utils/logger_level.utils";

mongoose.Promise = global.Promise;
mongoose.set("debug", process.env.DEBUG !== undefined);
mongoose.set('strictQuery', false);

const opts = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  keepAlive: true,
  keepAliveInitialDelay: 300000,
  autoIndex: config.mongo.autoIndex,
  serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
};

class MongoConnection {
  private static _instance: MongoConnection;

  private _mongoServer?: MongoMemoryServer;

  static getInstance(): MongoConnection {
    if (!MongoConnection._instance) {
      MongoConnection._instance = new MongoConnection();
    }
    return MongoConnection._instance;
  }

  public async open(): Promise<void> {
    let mongoDBConnected = false;
    try {
      if (config.mongo.url === "inmemory") {
        LOGGER.debug("connecting to inmemory mongo db");
        this._mongoServer = await MongoMemoryServer.create();
        const mongoUrl = this._mongoServer.getUri();
        await mongoose.connect(mongoUrl, opts);
      } else {
        LOGGER.debug("connecting to mongo db: " + config.mongo.url);
        try {
          await mongoose.connect(config.mongo.url, opts);
        } catch (error) {
          LOGGER.error(`Mongo: disconnected (database.open): ${error}`);
        }
      }

      mongoose.connection.on("connected", () => {
        mongoDBConnected = true;
        LOGGER.info("Mongo: connected");
      });

      mongoose.connection.on("disconnected", () => {
        mongoDBConnected = false;
        LOGGER.error("Mongo: disconnected");
      });

      mongoose.connection.on("error", (error) => {
        if (mongoDBConnected) {
          LOGGER.error(`Mongo:  ${String(error)}`);
          if (error.name === "MongoNetworkError") {
            setTimeout(function () {
              mongoose.connect(config.mongo.url, opts).catch(() => {});
            }, 5000);
          }
        }
      });
    } catch (error) {
      LOGGER.error(`database.open: ${error}`);
      throw error;
    }
  }

  public async close(): Promise<void> {
    try {
      await mongoose.disconnect();
      if (config.mongo.url === "inmemory") {
        await this._mongoServer!.stop();
      }
    } catch (error) {
      LOGGER.error(`database.open: ${error}`);
      throw error;
    }
  }
}

export default MongoConnection.getInstance();
