import express, { json } from "express";
import * as OpenApiValidator from "express-openapi-validator";
import morgan from "morgan";
import morganBody from "morgan-body";
import { connector } from "swagger-routes-express";

import * as api from "@mf/api/controllers";
import config from "@mf/config";
import apiConfig from "@mf/config/app.config";
import { LOGGER_DEV_EXPRESS } from "@mf/utils/logger_dev_express.utils";
import LOGGER from "@mf/utils/logger_level.utils";

let apiServer: any;

export async function createApiServer() {
  const SUCCESS_MESSAGE = "API is working!!!";

  apiServer = express();
  apiServer.use(json());

  setMorganLogger(apiServer);

  setOpenApiServerConnector();

  setErrorHandlingForInvalidRequest();

  apiServer.get("/", (_request: any, _response: any) => {
    /* istanbul ignore file */
    _response.send(SUCCESS_MESSAGE);
  });

  return apiServer;
}

function setErrorHandlingForInvalidRequest() {
  const ERROR_TYPE = "request_validation";
  apiServer.use(
    (
      _error: any,
      _request: express.Request,
      _response: express.Response,
      _next: express.NextFunction
    ) => {
      _response.status(_error.status).json({
        error: {
          type: ERROR_TYPE,
          message: _error.message,
          errors: _error.errors,
        },
      });
    }
  );
}

function setOpenApiServerConnector() {
  const yamlSpecFile = apiConfig.yamlSpecFile;
  const apiDefinition = apiConfig.apiDefinition;
  const apiSummary = apiConfig.apiSummary;
  LOGGER.info(apiSummary);

  const validatorOptions = {
    apiSpec: yamlSpecFile,
    validateRequests: true,
    validateResponses: true,
  };

  apiServer.use(OpenApiValidator.middleware(validatorOptions));

  const connect = connector(api, apiDefinition, {
    onCreateRoute: (method: string, descriptor: any[]) => {
      descriptor.shift();
      LOGGER.verbose(
        `${method}: ${descriptor.map((desc: any) => desc.name).join(", ")}`
      );
    },
    security: {
      bearerAuth: api.auth,
    },
  });

  connect(apiServer);
}

function setMorganLogger(_apiServer: any) {
  const MORGAN_METHOD =
    ":method :url :status :response-time ms - :res[content-length]";
  _apiServer.use(morgan(MORGAN_METHOD));
  morganBody(_apiServer);
  _apiServer.use(LOGGER_DEV_EXPRESS);

  /* istanbul ignore next */
  if (config.morganLogger) _apiServer.use(morgan(MORGAN_METHOD));
  /* istanbul ignore next */
  if (config.morganBodyLogger) morganBody(_apiServer);
  /* istanbul ignore next */
  if (config.mfDevLogger) _apiServer.use(LOGGER_DEV_EXPRESS);
}