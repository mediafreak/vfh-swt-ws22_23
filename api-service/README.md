### Table of Contents

- [Hintergrund zu ESA AOP \& Microservices](#hintergrund-zu-esa-aop--microservices)
- [Projekt Setup](#projekt-setup)
- [Installation](#installation)
- [Projekt Status/Funktionalitaeten](#projekt-statusfunktionalitaeten)
- [ToDos](#todos)
- [License](#license)

# Hintergrund zu ESA AOP & Microservices

[:arrow_up_small: top](#table-of-contents)

Auf Basis des [Tutorials](https://medium.com/@losikov/backend-api-server-development-with-node-js-from-scratch-to-production-fe3d3b860003) von Alex Losikov wird in diesem Projekt eine User Rest API als Microservice aufgebaut.

Die Beschreibung der API erfolgt über Swagger, OpenAPI Specification, Version 3.x. Sie dient als Grundlage für Tests mit Jest und das Routing mit Express (OpenApi-Validierung). Für das Speichern der Daten über <code>curl</code> in einer Datenbank enthält das Projekt ein Skript zum Starten einer MongoDB-Instanz im Docker- oder Podman-Container. 

# Projekt Setup

[:arrow_up_small: top](#table-of-contents)

| Kategorie | Link |  
| -------  | - | 
| Codebasis|  [TypeScript](https://www.typescriptlang.org/)| 
| Laufzeitumgebung  |  [Node.js](https://nodejs.org/)| 
| Backend Framework  | [Express.js](https://expressjs.com/) | 
| Versionierung| [Git](https://git-scm.com/) mit [GitLab](https://gitlab.com/)| 
| Package-Manager  | [Yarn](https://yarnpkg.com/) | 
| Logging | [Morgan](https://expressjs.com/en/resources/middleware/morgan.html) und [MorganBody](https://github.com/sirrodgepodge/morgan-body#readme)| 
| Script-Monitoring  | [Nodemon](https://nodemon.io/) | 
| Testing, Clean Code| [Jest](https://jestjs.io/), [Postman](https://www.postman.com/downloads/), [SonarCloud](https://sonarcloud.io/) (SonarQube) | 
| Umgebungsvariablen| [dotenv](https://github.com/keithmorris/node-dotenv-extended#readme) | 
| Containervirtualisierung | [Docker](https://www.docker.com/) oder [Podman](https://podman.io/)| 
| NoSQL-Datenbank | [MongoDB](https://www.mongodb.com/)| 
| MongoDB GUI | [Studio 3T](https://studio3t.com/download/)| 
| API Editor für API Design  | [Swagger Editor](https://swagger.io/tools/swagger-editor/)| 
| IDE  | [VSCode](https://code.visualstudio.com/)| 

# Installation

[:arrow_up_small: top](#table-of-contents)

- Notwendige Tools installieren (siehe [Projekt-Setupp](#projekt-setup))
- Gitlab-Projekt klonen und mit <code>yarn install</code> Module aus <code>package.json</code> des Projekts installieren.
- Folgende Skripte stehen über die <code>package.json</code> unterhalb des Nodes Node <code>scripts</code> zur Verfügung. 
 
| Befehl | Phase | Erläuterung | 
| -------  | - | - |
| <code>yarn dev</code> | Entwicklung   | Bei Änderungen an der Codebasis wird Anwendung unter <code>./src/app.ts </code>neu gestartet.  | 
| <code>yarn test</code> | Testing| Testen des TypeScript-Codes, Testfiles unter  <code>./src/api/__tests__</code> |
| <code>yarn test:cov</code>   | Testen  | Testen des TypeScript-Code und Generierung von Code-Coverage Files | 
| <code>yarn build</code> | Deployment |   Wandelt Typescript in Javascript und legt Quellcode im Verzeichnis <code>./bin/app.js</code> ab. | 
| <code>yarn start</code> | Deployment |  Führt kompilierten Javascript-Code im Verzeichnis <code>./bin</code> aus | 

Wenn API-Service über Docker gestartet werden soll:
- Build des Projekts erstellen
    ```bash
    yarn build
    ```
- Image auf Basis des mitgelieferten <code>Dockerfiles</code> erstellen
    ```bash
    docker build -t api-service . 
    ```
- Docker starten, 192.XXX.X.XXX muss durch IP-Adresse des Hostrechner ersetzt werden (ifconfig/ipconfig)
    ```bash
    docker run --rm -p 3070:3070 --name api-service -e MONGO_URL=mongodb://192.XXX.X.XXX/mediafreak \
    api-service
    ```


# Projekt Status/Funktionalitaeten

[:arrow_up_small: top](#table-of-contents)

Die ist ein laufendes Projekt von Jenny Pilz im Wintersemester 2022/23. 

Mit Stand vom 15.01.2023 stehen nachfolgende Funktionalitäten der API zur Verfügung. Diese basieren auf den Tutorial 2 bis 5 der Reihe [Backend API Server Development with Node.js from Scratch to Production](https://medium.com/@losikov/backend-api-server-development-with-node-js-from-scratch-to-production-fe3d3b860003) von Alex Losikov.

Bei Verwendung des Docker-Image muss <code>localhost</code> durch die IP-Adresse des Hostrechner ersetzt werden.

- Willkommenstext für User über

    ```bash
    curl -X 'GET' \
        'http://localhost:3070/api/v1/welcome?name=Jenny' \
        -H 'accept: application/json'
    ```

    ```bash
    curl -X 'GET' \
        'http://localhost:3070/api/v1/welcome' \
        -H 'accept: application/json'
    ```

- Goodbye-Text für gültigen Bearer-Token (Bearer-Authentication) und entsprechende Fehlermeldungen als Response bei falschen Bearer-Token

    ```bash
    curl 'GET' -v -H "Authorization: Bearer fakeToken" \
        "http://localhost:3070/api/v1/goodbye"
    ```

    ```bash
    curl 'GET' -v -H "Authorization: Bearer wrongToken" \
        "http://localhost:3070/api/v1/goodbye"
    ```

    ```bash
    curl 'GET' -v "http://localhost:3070/api/v1/goodbye"
    ```

- Erstellen eines Users durch unautorisierten User. Dabei Überprüfung, ob User bereits existiert oder Datensatz unvollständig und entsprechende Response:

    ```bash
    curl  -d  '{"email": "laub@froesche.com",  "password": "laub1frosch-GrU3n", "firstName": "Laub", "lastName": "von Frosch"}'  -H 'Content-Type: application/json' \ 
    http://localhost:3070/api/v1/public/user
    ```

# ToDos

[:arrow_up_small: top](#table-of-contents)

- Authenfizierung mit JWT-Tocken
- Erweiterung bzw. Vervollständigen der beschriebenen API-Calls (openapi.yaml)
- Refactoring nach CleanCode CheatSheet
- Aktualisieren der verwendete Bibliotheken (z.B. Jest: Migration von 26.x auf 29.x, [yaml.js](https://github.com/jeremyfa/yaml.js/tree/master)): Migration zu [js-yaml](https://github.com/nodeca/js-yaml))

# License

[:arrow_up_small: top](#table-of-contents)

MIT