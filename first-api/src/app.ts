/* simple user rest api
Inspired by tutorial from Bhargav Bachina
Link: https://medium.com/bb-tutorials-and-thoughts/how-to-write-production-ready-node-js-rest-api-javascript-version-db64d3941106

Refactoring by Jenny Pilz
See the changes based on the commits
*/
import express, { json } from 'express';
import apiRouter from './api/api.router';
import { ConsoleLogger } from './utils/logger';

const PORT = 3070;
const LOGGER = new ConsoleLogger();

let app = express();
app.disable("x-powered-by");

app.use(json());
app.use('/api/v1', apiRouter);

app.get('/', (_request, response) => {
  LOGGER.log('App is working');
  response.send('<h2>App is working</h2>');
});

app.get('*', (_request, response) => {
  LOGGER.info('App works for other routes');
  response.send('App works for other routes');
});

app.listen(PORT, () => {
  LOGGER.info('API is listening on port', PORT);
});