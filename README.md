# vfh-swt-ws22_23



## Aufgabensammlung für das Modul "Moderne Softwareentwicklung" (WS 2022/23)

Dieses Repository ist eine Studentarbeit von Jenny Pilz. Sie beinhaltet die Ergebnisse der Aufgaben zu 
- Metriken
- Clean Code Development
- DVCS mit GitLab
- Buildmanagement
- AOP & Microservices
 
Die Code-Basis ist JavaScript/TypeScript sowie Java

## Projekte
- First-API
  - NodeJS-Projekt auf Basis Typescript unter Verwendung des Express-Framework
  - Clean Code, Metriken mit Sonarcloud, Anwendung der verschiedenen Git-Command (siehe Commits)
  - Auf Basis des [Tutorials](https://medium.com/bb-tutorials-and-thoughts/how-to-write-production-ready-node-js-rest-api-javascript-version-db64d3941106) von Bhargav Bachina
- ChattyBotBazel
  - Java mit Maven-Bibliotheken
  - Build-Management mit Bazel
- ChattyBotMaven
  - Java mit Maven-Bibliotheken
  - Build-Management mit Maven
- API-Service
  -  API Design mittels Swagger, OpenApi, 3.x
  -  NodeJS-Projekt auf Basis Typescript unter Verwendung des Express-Framework, Jest etc.
  -  MongoDB über Docker-Container
  -  Auf Basis des [Tutorials](https://medium.com/@losikov/backend-api-server-development-with-node-js-from-scratch-to-production-fe3d3b860003) von Alex Losikov


## License
MIT

## Project status
Laufendes Studentenprojekt von Jenny Pilz im Wintersemester 2022/23